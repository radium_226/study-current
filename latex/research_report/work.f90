   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, Nb = 0, Nc = 0, Mb = 0, Mc = 0, i = 0, j = 0
   real(R_), allocatable   :: B(:,:), C(:,:)

   open (file=input_file, encoding=E_, newunit=In)
      read (In, *) Nb, Mb
      allocate (B(Nb, Mb))
      do i = 1, Nb
         do j = 1, Mb
            read (In, '(f5.1)', advance='no') B(i, j)
         end do
         read (In, *)
      end do
      
      read (In, *) Nc, Mc
      allocate (C(Nc, Mc))
      do i = 1, Nc
         do j = 1, Mc
            read (In, '(f5.1)', advance='no') C(i, j)
         end do
         read (In, *)
      end do
   close (In)

   !print *, MaxRowNum(B)
   !print *, MaxRowNum(C)

   open (file=output_file, encoding=E_, newunit=Out)
      write(Out, *) MaxRowNum(B), MaxRowNum(C)
      if(MaxRowNum(B) == MaxRowNum(C)) then
         write (Out, *) "Maximal values in a same rows"
      else  
         write (Out, *) "Maximal values in a different rows"
      end if
   close (Out)
contains
   pure function MaxRowNum(A) result(coord)
      !real(R_), allocatable     :: A(:,:)
      real(R_), intent(in)     :: A(:,:)
      real(R_)                  :: MaxVal
      integer                   :: N, M, coord, i, j

      N = size(A,dim=1)
      M = size(A,dim=2)
     
      MaxVal = A(1,1)
      coord = 1
      do i=1,N
         do j=1,M
            if(MaxVal < A(i,j)) then
               MaxVal = A(i,j)
               coord = i
            end if
         end do
      end do
      

   end function
end
