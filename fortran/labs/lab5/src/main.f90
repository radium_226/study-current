   use Environment
   use Process
   use IO

   implicit none
   character(:), allocatable                       :: input_file, output_file

!   character(kind=CH_), allocatable   :: queue(:, :)
!   character(SURNAME_LEN, kind=CH_)                :: queue(AMOUNT) = CH__"123"
   type(record), pointer                           :: tree

   input_file  = "../data/F1"
   output_file = "output.txt"
   
   tree => P1_read_tree(input_file)
!   call print_tree(output_file, tree)

   call print_branch_len(output_file, tree)


!   call P2_sort(queue) 
!   call P3_print_queue(output_file, queue)
end program
