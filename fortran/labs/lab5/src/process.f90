module Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none

   type record
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      type(record), pointer               :: left => null()
      type(record), pointer               :: right => null()
   end type record
contains
   recursive subroutine place_elem_in_tree(elem, tree)
      type(record), pointer, intent(inout) :: tree
      type(record), target, intent(in)    :: elem
      if(.not. associated(tree)) then
         !if no current tree elem, place here
         tree => elem
      else
         !if current elem exist, comparing and re-calling self
         if(elem%surname > tree%surname) then
            call place_elem_in_tree(elem, tree%right)
         else if(elem%surname < tree%surname) then
            call place_elem_in_tree(elem, tree%left)
         end if
      end if
   end subroutine place_elem_in_tree
end module process
