module IO
   use Environment
   use Process

   implicit none
   
contains
   function P1_read_tree(input_file) result (tree)
      character(*), intent(in)               :: input_file
      type(record), pointer                  :: tree
      integer                                :: in = 0

      open(file=input_file, encoding=E_, newunit=in)
         call tree_init(in, tree)   !root init
         call read_record(in, tree) !fill tree
      close(in)
   end function P1_read_tree
   
   subroutine tree_init(In, tree)
      type(record), pointer, intent(out) :: tree
      integer, intent(in)                  :: In
      integer  IO
      
      allocate (tree)
      read (In, '(a)', iostat=IO) tree%Surname
      call Handle_IO_status(IO, "reading line from file")
   end subroutine tree_init

   recursive subroutine Read_record(In, tree)
      type(record), pointer, intent(inout) :: tree
      type(record), pointer                :: elem
      integer, intent(in)                  :: In
      integer  IO
      
      allocate (elem)
      read (In, '(a)', iostat=IO) elem%Surname
      call Handle_IO_status(IO, "reading line from file")

      !print *, 1
      !placing element into tree and re-call read
      if (IO == 0) then
         call place_elem_in_tree(elem, tree)
         call Read_record(In, tree)
      else
         deallocate (elem)
         nullify (elem)
      end if
   end subroutine Read_record

   subroutine print_branch_len(output_file, tree)
      character(*), intent(in)               :: Output_File
      type(record), pointer, intent(in)      :: tree
      integer                                :: out = 0

      open(file=output_file, encoding=E_, newunit=out, position='append')
         call print_leaf_path(out, tree, 0)
      close(out)
   end subroutine print_branch_len

   recursive subroutine print_leaf_path(out, tree, level)
      type(record), pointer, intent(in)      :: tree
      integer                                :: level, out
      
      write(out, '(t' // level + 1 // ', a)', advance='no') tree%surname

      if(.not. (associated(tree%left) .and. associated(tree%right))) then
!         write(out, '(t' // level + 1 // ', i0)') level
         write(out, *) level
      end if

      if(associated(tree%left)) then
         write(out, '(t' // level + 1 // ', a)', advance='no') "L->"
         call print_leaf_path(out, tree%left, level + 1)
      end if     

      if(associated(tree%right)) then
         write(out, '(t' // level + 1 // ', a)', advance='no') "R->"
         call print_leaf_path(out, tree%right, level + 1)
      end if
   end subroutine print_leaf_path
end module IO 
