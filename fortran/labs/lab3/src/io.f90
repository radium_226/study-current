module IO
   use Environment
   use Process

   implicit none
   
contains
   function P1_read_queue(input_file) result (queue)
      character(*), intent(in)               :: input_file
      type(record), pointer                  :: queue
      integer                                :: in = 0

      open(file=input_file, encoding=E_, newunit=in)
         call read_record(in, queue)
      close(in)
   end function P1_read_queue
   
   recursive subroutine Read_record(In, rec)
      type(record), pointer, intent(out) :: rec
      integer, intent(in)                 :: In
      integer  IO
      
      allocate (rec)
      read (In, '(a)', iostat=IO) rec%Surname
      call Handle_IO_status(IO, "reading line from file")
      if (IO == 0) then
          call Read_record(In, rec%next)
      else
         deallocate (rec)
         nullify (rec)
      end if
   end subroutine Read_record

   subroutine P3_print_queue(output_file, queue)
      character(*), intent(in)               :: Output_File
      type(record)                           :: queue
      integer                                :: out = 0

      logical :: exist

      inquire(file=output_file, exist=exist)
      if (exist) then
         open(file=output_file, encoding=E_, newunit=out, position='append')
      else
         open(file=output_file, encoding=E_, newunit=out)
      end if
         write(out, '(a)') "--------------------------------------"
         call print_record(out, queue)
         write(out, '(a)') "--------------------------------------"
      close(out)
   end subroutine P3_print_queue

   recursive subroutine print_record(out, rec)
      integer, intent(in)                    :: out
      type(record), intent(in)               :: rec
      integer                                :: io = 0 

      write(out, '(a)', iostat=IO) (rec%surname)
      call Handle_IO_status(IO, "writing queue")

      if(associated(rec%next)) &
         call print_record(out, rec%next)
   end subroutine print_record
end module IO 
