module Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none

   type record
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      type(record), pointer               :: next => null()
   end type record
contains
   recursive pure subroutine P2_Sort(current)
      type(record), pointer, intent(inout)         :: current

      call Choose(current, current, current%next)
      if (associated(current%next)) &
         call P2_sort(current%next)
   end

   recursive pure subroutine Choose(unsorted, min, current)
      type(record), pointer, intent(inout)         :: unsorted, min, current
      type(record), pointer                        :: tmp

      if (.not. associated(current)) then
!         менять местами unsorted и min, если они не ссылаются на один объект
         if(.not. associated(unsorted, target=min)) then
            tmp => min
            min => min%next
            tmp%next => unsorted
            unsorted => tmp
         end if
      else if (current%surname < min%surname) then
         call Choose(unsorted, current, current%next)
      else
         call Choose(unsorted, min, current%next)
      end if
   end subroutine Choose
end module process
