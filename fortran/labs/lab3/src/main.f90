   use Environment
   use Process
   use IO

   implicit none
   character(:), allocatable                       :: input_file, output_file

!   character(kind=CH_), allocatable   :: queue(:, :)
!   character(SURNAME_LEN, kind=CH_)                :: queue(AMOUNT) = CH__"123"
   type(record), pointer                           :: queue

   input_file  = "../data/F1"
   output_file = "output.txt"
   
   queue => P1_read_queue(input_file)
   call P3_print_queue(output_file, queue)
   call P2_sort(queue) 
   call P3_print_queue(output_file, queue)
end program
