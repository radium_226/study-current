   use Environment
   use Process
   use IO

   implicit none
   character(:), allocatable                       :: input_file, output_file

!   character(kind=CH_), allocatable   :: queue(:, :)
!   character(SURNAME_LEN, kind=CH_)                :: queue(AMOUNT) = CH__"123"

   type(polynom), pointer              :: P, Q
   real(R_)                            :: Px = 0, Qx = 0, Rx = 0

   input_file  = "../data/input.txt"
   output_file = "output.txt"
   
   P => read_polynom(input_file, 1)
   Q => read_polynom(input_file, 2)

   call print_polynom(output_file, P, "P=")
   call print_polynom(output_file, Q, "Q=")

   Px = polynom_calc(P, 5.0)
   Qx = polynom_calc(Q, 5.0)

   Rx = Px * Qx
   call print_values(output_file, Px, Qx, Rx)
end program
