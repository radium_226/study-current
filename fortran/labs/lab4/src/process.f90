module Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none

   type polynom
      real(R_)               :: coef
      real(R_)               :: exp
      type(polynom), pointer :: next => null()
   end type polynom
contains
   pure recursive subroutine Parse_buffer(buffer, record)
      character(*, kind=CH_), intent(in)     :: buffer
      type(polynom), pointer, intent(inout)     :: record
      integer                                :: sep1, sep2
     
      allocate(record)

      sep1 = index(buffer, CH__"(")
      sep2 = index(buffer(sep1+1:), CH__")")+sep1

!      print *, buffer(sep1+1:sep2-1),"|", sep1, sep2, len(buffer, kind=CH_)
      
      call read_record(buffer(sep1+1:sep2-1), record)

      if(.not. sep2 == index(buffer, CH__")", back=.true.)) then
         call Parse_buffer(buffer(sep2+2:), record%next)
      end if
   end subroutine Parse_buffer
   
   pure subroutine Read_record(buffer, rec)
      type(polynom), pointer, intent(inout) :: rec
      character(*, kind=CH_), intent(in)    :: buffer
      allocate (rec)
      
      read (buffer, '(f3.1, f4.1)') rec%coef, rec%exp
   end subroutine Read_record

   recursive function polynom_calc(poly, x) result(res)
      type(polynom), pointer, intent(in)     :: poly
      real(R_), intent(in)                   :: x
      real(R_)                               :: res
      
      res = (poly%coef * X) ** poly%exp
      if(associated(poly%next)) then
         res = res + polynom_calc(poly%next, x)
      end if      
   end function polynom_calc
end module process
