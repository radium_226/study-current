module IO
   use Environment
   use Process

   implicit none
   
contains
   function read_polynom(input_file, line_number) result (poly)
      character(*), intent(in)               :: input_file
      integer, intent(in)                    :: line_number
      type(polynom), pointer                 :: poly
      integer                                :: in = 0, io = 0, i = 0
      character(30, kind=CH_)               :: buffer

      open(file=input_file, encoding=E_, newunit=in)
         if(line_number > 1) then
            do i=1, line_number - 1
               read(in, *)
            end do
         end if
         read (In, '(a)', iostat=io) buffer
         call parse_buffer(buffer, poly)
      close(in)
   end function read_polynom
   
   subroutine print_polynom(output_file, poly, label)
      character(*), intent(in)   :: Output_File, label
      type(polynom), intent(in)  :: poly
      integer  :: Out
      open (file=Output_File, encoding=E_, newunit=Out, position='append')
         write (out, '(a)', advance='no') label
         call print_polynom_record(Out, poly)
      close (Out)
   end subroutine print_polynom
   
   recursive subroutine print_polynom_record(Out, rec)
      integer, intent(in)        :: Out
      type(polynom), intent(in)  :: rec
      integer  :: IO

!      print *, rec%coef, rec%exp
      write (Out, '(f3.1, a, f3.1)', iostat=IO, advance='no')  rec%coef, "*X^", rec%exp
      call Handle_IO_status(IO, "writing student to file")
      if (Associated(rec%next)) then
         write(out, '(a)', advance='no') "+"
         call print_polynom_record(Out, rec%next)
      else
         write(out, *)
      end if
   end subroutine print_polynom_record
   
   subroutine print_values(output_file, Px, Qx, Rx)
      character(*), intent(in)   :: Output_File
      real(R_), intent(in)       :: Px, Qx, Rx
      integer  :: Out
      open (file=Output_File, encoding=E_, newunit=Out, position='append')
         write (out, *) "Px = ", Px
         write (out, *) "Qx = ", Qx
         write (out, *) "Rx = ", Rx
      close (Out)
   end subroutine print_values

end module IO 
