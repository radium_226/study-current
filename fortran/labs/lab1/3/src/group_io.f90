module Group_IO
   use Environment
   use Group_Process

   implicit none
   
contains
   subroutine Create_data_file(Input_File, Data_File)
      character(*), intent(in)   :: Input_File, data_file
      
      type(student)              :: stud
      integer                    :: In, Out, IO, i, recl
      character(:), allocatable  :: stud_form
      
      open (file=Input_File, encoding=E_, newunit=In)
      recl = (SURNAME_LEN)*CH_ + I_ + CH_
      open (file=Data_File, form='unformatted', newunit=Out, access='direct', recl=recl)
      stud_form = '(a, i4, 1xa)'
      do i = 1, STUD_AMOUNT
         read (In, stud_form, iostat=IO) stud
         call Handle_IO_status(IO, "reading formatted class list, line " // i)
         
         write (Out, iostat=IO, rec=i) stud
         call Handle_IO_status(IO, "creating unformatted file with class list, record " // i)
      end do
      close (In)
      close (Out)
   end subroutine Create_data_file

   function Read_class_list(Data_File) result(Group)
      type(student)                 Group(STUD_AMOUNT)
      character(*), intent(in)   :: Data_File

      integer In, IO, recl
      
      recl = ((SURNAME_LEN)*CH_ + I_ + CH_) * STUD_AMOUNT
      open (file=Data_File, form='unformatted', newunit=In, access='direct', recl=recl)
      read (In, iostat=IO, rec=1) Group
      call Handle_IO_status(IO, "reading unformatted class list")
      close (In)
   end function Read_class_list
 
   subroutine Output_class_list(Output_File, Group)
      character(*), intent(in)   :: Output_File
      type(student), intent(in)  :: Group(:)

      integer                    :: Out, IO
      character(:), allocatable  :: stud_form
      
      open (file=Output_File, encoding=E_, newunit=Out)
      write (out, '(a)') "Initial list:"
!      stud_form = '(3(a, 1x), ' // MARKS_AMOUNT // 'i1, i2)'
      stud_form = '(a, i4, 1xa)'
      write (Out, stud_form, iostat=IO) Group
      call Handle_IO_status(IO, "writing class list")
      close (Out)
   end subroutine Output_class_list

   subroutine PrintStudent(Output_File, Stud, Label)
      character(*), intent(in)   :: Output_File, Label
      type(student), intent(in)  :: Stud
      integer                    :: Out
      character(:), allocatable  :: format
      
      format = '(a, i4, 1xa)'
      
      open (file=Output_File, encoding=E_, newunit=Out, position='append')
         write (out, '(a)') Label 
         !write (out, *) getOldest(Group, Int(z'041C'))
         write (out, format) Stud
      close (Out)
   end subroutine PrintStudent
   
!   subroutine PrintYoungerFemale(Output_File, Group)
!      character(*), intent(in)   :: Output_File
!      type(student), intent(in)  :: Group(:)
!      integer                    :: Out
!      character(:), allocatable  :: format
!      
!      format = '(a, i4, 1xa)'
!      
!      open (file=Output_File, encoding=E_, newunit=Out, position='append')
!         write (out, '(a)') "Самая молодая женщина:"
!         !write (out, *) getYounger(Group, Int(z'0416'))
!         write (out, format) get(Group, Int(z'0416'), .false.)
!      close (Out)
!   end subroutine PrintYoungerFemale


end module Group_IO 
