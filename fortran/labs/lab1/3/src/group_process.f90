module Group_Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none
   integer, parameter :: STUD_AMOUNT   = 5
   integer, parameter :: SURNAME_LEN   = 15

   ! Структура данных для хранения данных о студенте.
   type student
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      integer                             :: bYear  = 0
      character(kind=CH_)                 :: Gender                  = CH__""
   end type student
   
contains
   pure function Get(Group, GenderMark, AgeType) result(res)
      type(student), intent(in)           :: Group(:)
      type(student)                       :: res
      integer, intent(in)                 :: GenderMark
      logical, intent(in)                 :: AgeType ! true - oldest, false - younger
      logical, allocatable                :: Marked(:)
      
      Marked = Group%Gender == Char(GenderMark, CH_)

      if(AgeType) then
         res = Group(MinLoc(Group%bYear, dim=1, mask=Marked))
      else
         res = Group(MaxLoc(Group%bYear, dim=1, mask=Marked))
      end if   

   end  function Get

end module group_process
