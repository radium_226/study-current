   use Environment

   implicit none
   integer, parameter         :: STUD_AMOUNT = 5, SURNAME_LEN = 15 
   character(:), allocatable  :: input_file, output_file, format

   character(SURNAME_LEN, kind=CH_)                :: Surnames(STUD_AMOUNT) = CH__""
   
   character(kind=CH_)                             :: Gender(STUD_AMOUNT) = CH__"1"
   
   integer                                         :: Age(STUD_AMOUNT) = 0
   integer                                         :: tmp(1) = 0

   logical, allocatable                            :: Is_A_Male(:), Is_A_Female(:)
   integer                                         :: OldestMale = 0, YoungerFemale = 0

   integer :: In, Out, IO, i!, j

   input_file = "../data/class.txt"
   output_file = "output.txt"
   open (file=input_file, encoding=E_, newunit=In)
      format = '(a, i4, 1xa)'
      read (In, format, iostat=IO) (Surnames(i), Age(i), Gender(i), i = 1, STUD_AMOUNT)
   close (In)

   ! Обработка статуса чтения.
   Out = OUTPUT_UNIT
   open (Out, encoding=E_)
   select case(io)
      case(0)
      case(IOSTAT_END)
         write (Out, '(a)') "End of file has been reached while reading class list."
      case(1:)
         write (Out, '(a)') "Error while reading class list: ", io
      case default
         write (Out, '(a)') "Undetermined error has been reached while reading class list: ", io
   end select
   ! Вывод списка класса.
   open (file=output_file, encoding=E_, newunit=Out)
      write (out, '(a)') "Исходный список:"
      write (Out, format, iostat=IO) (Surnames(i), Age(i), Gender(i), i = 1, STUD_AMOUNT)
   close (Out)
   ! Обработка статуса записи.
   Out = OUTPUT_UNIT
   open (Out, encoding=E_)
   select case(io)
      case(0)
      case(IOSTAT_END)
         write (Out, '(a)') "End of file has been reached while writing class list."
      case(1:)
         write (Out, '(a)') "Error while writing class list: ", io
      case default
         write (Out, '(a)') "Undetermined error has been reached while writing class list: ", io
   end select

   ! Составление логической маски, соответствующей юношам.
   Is_A_Male       = Gender == Char(Int(z'041C'), CH_) 
   Is_A_Female      = .not. Is_A_Male
   
   !Поиск иднексов
   OldestMale = MinLoc(Age,dim=1, mask=Is_A_Male)
   
   tmp = MaxLoc(Age, Is_A_Female)
   YoungerFemale = tmp(1) 
   
   
   ! Вывод результата
   open (file=output_file, encoding=E_, newunit=Out, position='append')
      write (out, '(a)') "Самый пожилой мужчина:"
      write (Out, format, iostat=IO) Surnames(OldestMale), Age(OldestMale), Gender(OldestMale)
      write (out, '(a)') "Самая молодая женщина:"
      write (Out, format, iostat=IO) Surnames(YoungerFemale), Age(YoungerFemale), Gender(YoungerFemale)
   close (Out)
   ! Обработка статуса записи.
   Out = OUTPUT_UNIT
   open (Out, encoding=E_)
   select case(io)
      case(0)
      case(IOSTAT_END)
         write (Out, '(a)') "End of file has been reached while writing class list."
      case(1:)
         write (Out, '(a)') "Error while writing class list: ", io
      case default
         write (Out, '(a)') "Undetermined error has been reached while writing class list: ", io
   end select
end 
