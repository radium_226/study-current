module Group_IO
   use Environment
   use Group_Process

   implicit none
   
contains
   function Read_class_list(Input_File) result(ClassList)
      character(*), intent(in)               :: Input_File
      type(student), pointer                 :: ClassList
      integer  In

      open (file=Input_File, encoding=E_, newunit=In)
      call Read_student(In, ClassList)
      close (In)
   end function Read_class_list

   recursive subroutine Read_student(In, Stud)
      type(student), pointer, intent(out) :: Stud
      integer, intent(in)                 :: In
      integer  IO
      allocate (Stud)
      read (In, '(a, i4, 1x, a)', iostat=IO) stud%Surname, stud%bYear, stud%Gender
      call Handle_IO_status(IO, "reading line from file")
      if (IO == 0) then
          call Read_student(In, Stud%next)
      else
         deallocate (Stud)
         nullify (Stud)
      end if
   end subroutine Read_student

   subroutine Output_class_list(Output_File, ClassList)
      character(*), intent(in)   :: Output_File
      type(student), intent(in)  :: ClassList
      integer  :: Out
      
      open (file=Output_File, encoding=E_, newunit=Out)
      write (out, '(a)') "Initial list:"
      call Output_student(Out, ClassList)
      close (Out)
   end subroutine Output_class_list
   
   recursive subroutine Output_student(Out, Stud)
      integer, intent(in)        :: Out
      type(student), intent(in)  :: Stud
      integer  :: IO

      write (Out, '(a, i4, 1x, a)', iostat=IO) Stud%Surname, Stud%bYear, Stud%Gender
      call Handle_IO_status(IO, "writing student to file")
      if (Associated(Stud%next)) &
         call Output_student(Out, Stud%next)
   end subroutine Output_student

   subroutine PrintStudent(output_file, Stud, Label)
      character(*), intent(in)   :: Output_File, Label
      type(student), intent(in)  :: Stud
      integer                    :: Out
      character(:), allocatable  :: format
      
      format = '(a, i4, 1xa)'
   
      open (file=Output_File, encoding=E_, newunit=Out, position='append')
         write (out, '(a)') Label
         write (out, format) Stud%surname, Stud%byear, Stud%gender
      close (Out)
   end subroutine PrintStudent
end module Group_IO 
