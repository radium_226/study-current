module Group_Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none
   integer, parameter :: STUD_AMOUNT   = 5
   integer, parameter :: SURNAME_LEN   = 15

   ! Структура данных для хранения данных о студенте.
   type student
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      integer                             :: bYear  = 0
      character(kind=CH_)                 :: Gender                  = CH__""
      type(student), pointer              :: next => null()
   end type student
   
contains
   function get(List, GenderMark, AgeType) result(Stud)
      type(student), target               :: List
      integer, intent(in)                 :: GenderMark
      logical, intent(in)                 :: AgeType
      type(student), pointer              :: Stud

      do
         if(List%Gender == char(GenderMark, CH_)) then
            Stud => List
            exit
         else
            list = list%next
         end if
      end do

      if(AgeType) then
         call recMinYear(stud, stud, GenderMark)
      else
         call recMaxYear(stud, stud, GenderMark)
      end if
   end function get

!   recursive function getFirstOfGender(list, Mark) result (res)
!      type(student),target, intent(in)  :: List
!      integer, intent(in)        :: mark
!      type(student)              :: res
!
!      if(list%gender == char(Mark, CH_)) then
!         res = list
!      else
!         res = getFirstOfGender(list%next, Mark)
!      end if
!   end function getFirstOfGender
!
   recursive subroutine recMinYear(List, curMinElem, GenderMark)
      type(student), target, intent(in)                 :: List
      type(student), pointer, intent(inout)     :: curMinElem
      integer, intent(in)                       :: GenderMark 

      if(list%gender == char(GenderMark, CH_) .and. List%byear < curMinElem%byear) &
         curMinElem => List

      if(associated(list%next)) then
         call recMinYear(List%next, curMinElem, GenderMark)
      end if
   end subroutine recMinYear
   
   recursive subroutine recMaxYear(List, curMaxElem, GenderMark)
      type(student), target, intent(in)  :: List
      type(student), pointer, intent(inout)  :: curMaxElem
      integer, intent(in)        :: GenderMark 

      if(list%gender == char(GenderMark, CH_) .and. List%byear > curMaxElem%byear) &
         curMaxElem => List

      if(associated(list%next)) then
         call recMaxYear(List%next, curMaxElem, GenderMark)
      end if
   end subroutine recMaxYear

!   recursive subroutine GetGenderList(List, Mark)
!      type(student), intent(inout)  :: List
!      integer, intent(in)           :: Mark
!
!      if(.not. list%gender == char(mark, CH_)) then
!         print *, list%gender, list%surname
!         if(associated(list%next)) &
!            list = list%next
!      end if
!
!      if(associated(list%next)) then
!         call getGenderList(list%next, Mark)
!      end if
!   end subroutine getGenderList
end module group_process
