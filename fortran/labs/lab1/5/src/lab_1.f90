   use Environment
   use Group_Process
   use Group_IO

   implicit none
   character(:), allocatable :: input_file, output_file

   type(student), pointer  :: GroupList   => Null()   ! Список группы.

   input_file  = "../data/class.txt"
   output_file = "output.txt"
   
   GroupList => Read_class_list(input_file)

   call Output_class_list(output_file, GroupList)
   
   call PrintStudent(output_file, &
        get(GroupList, int(z'041C'), .true.), "Самый пожилой мужчина")

   call PrintStudent(output_file, &
        get(GroupList, int(z'0416'), .false.), "Самая молодая женщина")
end program
