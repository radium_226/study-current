   use Environment

   implicit none
   integer, parameter         :: STUD_AMOUNT = 5, SURNAME_LEN = 15 
   character(:), allocatable  :: input_file, output_file

   character(kind=CH_)        :: Surnames(STUD_AMOUNT, SURNAME_LEN)  = CH__"", &
                                 Genders(STUD_AMOUNT)                   = CH__""
   
   integer                    :: Ages(STUD_AMOUNT) = 0

   input_file = "../data/class.txt"
   output_file = "output.txt"


   call Read_list(input_file, Surnames, Ages, Genders)

   call Output_list(output_file, Surnames, Ages, Genders)

!   call Print_Oldest_Male(output_file, Surnames, Ages, Genders)
   
!   call Print_Younger_Female(output_file, Surnames, Ages, Genders)

   call print(output_file, Surnames, Ages, Genders, &
          get(Ages, Genders, int(z'041C'), .true.), "Самый пожилой мужчина");
   call print(output_file, Surnames, Ages, Genders, &
          get(Ages, Genders, int(z'0416'), .false.), "Самая молодая женщина");

   contains
      subroutine Read_list(Input_File, Surnames, Ages, Genders)
         character(*)         Input_File
         character(kind=CH_)  Surnames(:, :), Genders(:)
         integer              Ages(:)
         intent (in)          Input_File
         intent (out)         Surnames, Ages, Genders
   
         integer In, IO, i
         character(:), allocatable  :: format
         
         open (file=Input_File, encoding=E_, newunit=In)
            !format = '(' // SURNAME_LEN // 'a1, 1x, ' // INITIALS_LEN // 'a1, 1x, a, 1x, ' // &
            !   MARKS_AMOUNT // 'i1, f5.2)'
            format = '(' // SURNAME_LEN // 'a, i4, 1xa)'
            read (In, format, iostat=IO) (Surnames(i, :), Ages(i), Genders(i), i = 1, STUD_AMOUNT)
            call Handle_IO_status(IO, "reading class list")
         close (In)
      end subroutine Read_list
   
      subroutine Output_list(Output_File, Surnames, Ages, Genders)
         character(*)         Output_File
         character(kind=CH_)  Surnames(:, :), Genders(:)
         integer              Ages(:)
         intent (in)          Output_File, Surnames, Ages, Genders
   
         integer                    :: Out, i, IO
         character(:), allocatable  :: format
      
         open (file=output_file, encoding=E_, position='rewind', newunit=Out)
            format = '(' // SURNAME_LEN // 'a, i4, 1xa)'
            write (Out, format, iostat=IO) &
               (Surnames(i, :), Ages(i), Genders(i), i = 1, STUD_AMOUNT)
            call Handle_IO_status(IO, "writing class list")
         close (Out)
      end subroutine Output_list

      pure function get(Ages, Genders, GenderMark, AgeType) result (id) 
         !gender mark is a char code(z'041C' for male and z'0416' for female)
         !age type is boolean, true - oldest, false - younger
         character(kind=CH_),intent(in)      :: Genders(:)
         integer, intent(in)                 :: Ages(:)
         logical, allocatable                :: Marked(:)
         integer, intent(in)                 :: GenderMark
         logical, intent(in)                 :: AgeType
   
         integer                             :: id

         Marked = Genders == Char(GenderMark, CH_) 
         if(AgeType) then
            id = MinLoc(Ages, dim=1, mask=Marked)
         else 
            id = MaxLoc(Ages, dim=1, mask=Marked)
         end if
      end function get

      subroutine print(Output_file, Surnames, Ages, Genders, id, Label)
         character(*)         Output_File, Label
         character(kind=CH_)  Surnames(:, :), Genders(:)
         integer              Ages(:), id
         intent (in)          Output_File, Label, id, Surnames, Ages, Genders
   
         integer                    :: Out, IO
         character(:), allocatable  :: format
   
         open (file=output_file, encoding=E_, position='append', newunit=Out)
            write (out, '(/a)') Label 
            format = '(' // SURNAME_LEN // 'a, i4, 1xa)'
            write (Out, format, iostat=IO) &
               Surnames(id, :), Ages(id), Genders(id)
            call Handle_IO_status(IO, "writing class list")
         close (Out)
      end subroutine

!      pure function getOldestMale(Surnames, Ages, Genders) result (OldestMale)
!         character(kind=CH_)  Surnames(:, :), Genders(:)
!         integer              Ages(:)
!         logical, allocatable       :: Is_A_Male(:)
!   
!         integer                    :: Out, IO
!         integer(out)               :: OldestMale = 0
!
!         Is_A_Male       = Genders == Char(Int(z'041C'), CH_) 
!         OldestMale = MinLoc(Ages, dim=1, mask=Is_A_Male)
!      end function getOldestMale
!      
!      pure function getYoungerFemale(Surnames, Ages, Genders) result (YoungerFemale)
!         character(kind=CH_)  Surnames(:, :), Genders(:)
!         integer              Ages(:)
!         logical, allocatable       :: Is_A_Female(:)
!   
!         integer                    :: Out, IO
!         integer(out)               :: YoungerFemale = 0
!
!         Is_A_Female       = Genders == Char(Int(z'0416'), CH_) 
!         YoungerFemale = MaxLoc(Ages, dim=1, mask=Is_A_Female)
!      end function getYoungerFemale
      
!      subroutine Print_Oldest_Male(Output_file, Surnames, Ages, Genders)
!         character(*)         Output_File
!         character(kind=CH_)  Surnames(:, :), Genders(:)
!         integer              Ages(:)
!         intent (in)          Output_File, Surnames, Ages, Genders
!         character(:), allocatable  :: format
!         integer                    :: Out, IO, OldestMale = 0
!   
!         OldestMale = get(Ages, Genders, int(z'041C'), .true.)
!
!         open (file=output_file, encoding=E_, position='append', newunit=Out)
!            write (out, '(/a)') "Самый старый мужчина" 
!            format = '(' // SURNAME_LEN // 'a, i4, 1xa)'
!            write (Out, format, iostat=IO) &
!               Surnames(OldestMale, :), Ages(OldestMale), Genders(OldestMale)
!            call Handle_IO_status(IO, "writing class list")
!         close (Out)
!      end subroutine Print_Oldest_Male
!      
!      subroutine Print_Younger_Female(Output_file, Surnames, Ages, Genders)
!         character(*)         Output_File
!         character(kind=CH_)  Surnames(:, :), Genders(:)
!         integer              Ages(:)
!         intent (in)          Output_File, Surnames, Ages, Genders
!   
!         integer                    :: Out, IO, YoungerFemale = 0
!         character(:), allocatable  :: format
!   
!         YoungerFemale = get(Ages, Genders, int(z'0416'), .false.)
!
!         open (file=output_file, encoding=E_, position='append', newunit=Out)
!            write (out, '(/a)') "Самая молодая женщина" 
!            format = '(' // SURNAME_LEN // 'a, i4, 1xa)'
!            write (Out, format, iostat=IO) &
!               Surnames(YoungerFemale, :), Ages(YoungerFemale), Genders(YoungerFemale)
!            call Handle_IO_status(IO, "writing class list")
!         close (Out)
!      end subroutine Print_Younger_Female
end 
