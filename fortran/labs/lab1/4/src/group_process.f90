module Group_Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none
   integer, parameter :: STUD_AMOUNT   = 5
   integer, parameter :: SURNAME_LEN   = 15

   ! Структура данных для хранения данных о студенте.
   type student
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      integer                             :: bYear  = 0
      character(kind=CH_)                 :: Gender                  = CH__""
   end type student
   
contains
   pure recursive subroutine recMinYear(Arr, currMinLoc, currElemLoc)
      integer, intent(inout)  :: currMinLoc
      integer, intent(in)  :: Arr(:), currElemLoc
      integer              :: N
      N = size(Arr)
      
     if(currElemLoc <= N) then
        if(Arr(CurrElemLoc) < Arr(currMinLoc)) then
            currMinLoc = currElemLoc
        end if
        call recMinYear(Arr, currMinLoc, currElemLoc + 1)
     end if
   end subroutine recMinYear
   pure recursive subroutine recMaxYear(Arr, currMaxLoc, currElemLoc)
      integer, intent(inout)  :: currMaxLoc
      integer, intent(in)  :: Arr(:), currElemLoc
      integer              :: N
      N = size(Arr)
      
     if(currElemLoc <= N) then
        if(Arr(CurrElemLoc) > Arr(currMaxLoc)) then
            currMaxLoc = currElemLoc
        end if
        call recMaxYear(Arr, currMaxLoc, currElemLoc + 1)
     end if
   end subroutine recMaxYear
   
!   pure recursive function recMaxYear(Arr, currMaxLoc, currElemLoc) result (loc)
!      !implict none
!      integer, intent(in)  :: Arr(:), currMaxLoc, currElemLoc
!      integer              :: loc, N
!      N = size(Arr)
!      
!      if(N == 2) then
!         if(Arr(1) <= Arr(2)) then
!            loc = 1
!         else
!            loc = 2
!         end if
!      else
!         if(currElemLoc == N) then
!            loc = currMaxLoc
!         else
!            if(recMaxYear(Arr, currMaxLoc, currElemLoc + 1) < Arr(currMaxLoc)) then
!               loc = currElemLoc
!            end if
!         end if
!      end if
!   end function recMaxYear

!   pure function GetOldest(Group, GenderMark) result(res)
!      type(student), intent(in)           :: Group(:)
!      type(student)                       :: res
!      integer, intent(in)                 :: GenderMark
!      logical, allocatable                :: Marked(:)
!      
!      Marked = Group%Gender == Char(GenderMark, CH_)
!
!      res = Group(MinLoc(Group%bYear, dim=1, mask=Marked))
!   end  function GetOldest
!
!   pure function GetYounger(Group, GenderMark) result(res)
!      type(student), intent(in)           :: Group(:)
!      type(student)                       :: res
!      integer, intent(in)                 :: GenderMark
!      logical, allocatable                :: Marked(:)
!      
!      Marked = Group%Gender == Char(GenderMark, CH_)
!
!      res = Group(MaxLoc(Group%bYear, dim=1, mask=Marked))
!   end  function GetYounger
   
   pure function Get(Group, GenderMark, AgeType) result(res)
      type(student), intent(in)           :: Group(:)
      type(student)                       :: res
      integer, intent(in)                 :: GenderMark
      logical, intent(in)                 :: AgeType ! true - oldest, false - younger
      logical, allocatable                :: Marked(:)
      !integer, allocatable                :: packedArray(:)
      type(student), allocatable          :: MarkedGroup(:)
      integer                             :: Elem
      
      Marked = Group%Gender == Char(GenderMark, CH_)
      allocate (MarkedGroup(count(Marked)))

      MarkedGroup = Pack(Group, Marked)

      Elem = 1
      if(AgeType) then
         call recMinYear(MarkedGroup%bYear, Elem, 2)
      else
         call recMaxYear(MarkedGroup%bYear, Elem, 2)
      end if   
      res = MarkedGroup(Elem)

   end  function Get

end module group_process
