   use Environment
   use Group_Process
   use Group_IO

   implicit none
   character(:), allocatable :: input_file, output_file, data_file

   type(student)  :: Group(STUD_AMOUNT) ! Список группы.

   input_file  = "../data/class.txt"
   output_file = "output.txt"
   data_file   = "class.dat"
   
   call Create_data_file(input_file, data_file)
   
   Group = Read_class_list(data_file)

   call Output_class_list(output_file, Group)

   call PrintStudent(output_file, &
        get(Group, Int(z'041C'), .true.), "Самый пожилой мужчина")
   call PrintStudent(output_file, &
        get(Group, Int(z'0416'), .false.), "Самая молодая женщина")
end program
