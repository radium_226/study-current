   use Environment
   use Process
   use IO

   implicit none
   character(:), allocatable :: input_file, output_file, output_file2

   type(thefile), pointer     :: files   => Null()
   integer                 :: Amount      = 0

   input_file  = "../data/list.txt"
   output_file = "output_byname.txt"
   output_file2 = "output_bytype.txt"
   
   call Read_file_list(input_file, files, Amount)

   if (Amount > 0) then
      call Output_file_list(output_file, files)
      call Sort_file_list(files, Amount, .false.) !by name
      call Output_file_list(output_file, files)
      call Sort_file_list(files, Amount, .true.) !by type
      call Output_file_list(output_file2, files)
   end if
end program
