module IO
   use Environment
   use Process

   implicit none
   
contains
   subroutine Read_file_list(Input_File, List, Amount)
      character(*), intent(in)               :: Input_File
      type(thefile), pointer, intent(inout)  :: List
      integer, intent(out)                   :: Amount
      integer  In

      open (file=Input_File, encoding=E_, newunit=In)
      call Read_file(In, List, Amount)
      close (In)
   end subroutine Read_file_list

   recursive subroutine Read_file(In, File, Amount)

      type(thefile), pointer, intent(out)    :: File
      integer, intent(in)                 :: In
      integer, intent(inout)              :: Amount
      integer  IO, pos
      character(255, kind=CH_)            :: rawname = CH__""

      
      allocate (file)
      read (In, '(a)', iostat=IO) rawname

      print *, rawname

      pos = index(rawname, CH__".", .true.)
!      print *, rawname(1:pos-1), rawname(pos+1:len(rawname)), pos

      File%filename = rawname(1:pos - 1)
      File%filetype = rawname(pos+1:len(rawname))
      file%rawname = rawname

!      print *, file%filetype

      call Handle_IO_status(IO, "reading line from file")
      if (IO == 0) then
         Amount = Amount + 1
          call Read_file(In, file%next, Amount)
      else
         deallocate (file)
         nullify (file)
      end if
   end subroutine Read_file

   subroutine Output_file_list(Output_File, FileList)
      character(*), intent(in)   :: Output_File
      type(thefile), intent(in)  :: FileList
      integer  :: Out
      
      open (file=Output_File, encoding=E_, newunit=Out)
      write (out, '(a)') "Initial list:"
      call Output_filename(Out, FileList)
      close (Out)
   end subroutine Output_file_list
   
   recursive subroutine Output_filename(Out, File)
      integer, intent(in)        :: Out
      type(thefile), intent(in)  :: File
      integer  :: IO

      write (Out, '(a)', iostat=IO) File%rawname
      call Handle_IO_status(IO, "writing filename")
      if (Associated(File%next)) &
         call Output_filename(Out, file%next)
   end subroutine output_filename
end module IO 
