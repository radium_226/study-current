module Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none

   type thefile
      character(255, kind=CH_)     :: filename              = CH__""
      character(255, kind=CH_)     :: filetype              = CH__""
      character(255, kind=CH_)     :: rawname              = CH__""
      type(thefile), pointer       :: next => null()
   end type thefile
   
contains
   pure recursive subroutine Sort_file_list(FileList, N, SortType)
      type(thefile), pointer, intent(inout)  :: FileList
      integer, intent(in)                    :: N
      logical, intent(in)                    :: SortType ! true - by extension, false - by name

      call Drop_down(FileList, 1, N-1, SortType)
      
      if (N >= 3) &
         call Sort_file_list(FileList, N-1, SortType)
   end subroutine Sort_file_list

   pure recursive subroutine Drop_down(FileList, j, N, SortType)
      type(thefile), pointer  :: FileList
      integer, intent(in)                    :: j, N
      logical, intent(in)                    :: SortType ! true - by extension, false - by name

      if (Swap(FileList, SortType)) &
         call Swap_from_current(FileList)
      if (j < N) &
         call Drop_down(FileList%next, j+1, N, SortType)
   end subroutine Drop_down

   pure logical function Swap(Current, SortType)
      type(thefile), intent(in)  :: Current
      logical, intent(in)                    :: SortType ! true - by extension, false - by name

      Swap = .false.
      if(SortType) then
         if (Current%filetype > Current%next%filetype) then
            Swap = .true.
         end if
      else
         if (Current%filename > Current%next%filename) then
            Swap = .true.
         end if
      end if
   end function Swap
   
   pure subroutine Swap_from_current(Current)
      type(thefile), pointer  :: Current

      type(thefile), pointer  :: tmp_file
               
      tmp_file       => Current%next
      Current%next   => Current%next%next
      tmp_file%next  => Current
      Current        => tmp_file

   end subroutine Swap_from_current
end module process
