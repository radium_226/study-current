   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, M = 0, i = 0, j = 0
   real(R_), allocatable   :: A(:,:)
   logical, allocatable    :: Matched(:), Mask(:)
   integer, allocatable    :: Ind(:)


   open (file=input_file, encoding=E_, newunit=In)
   read (In, *) N, M
   allocate (A(N, M))
   
   allocate (Ind(N))
   Ind = [(i,i=1,N)]
   
   allocate (Matched(N))
   Matched = .false.

   allocate (Mask(N))
   Mask = .false.
   
   do i = 1, N
      do j = 1, M
         read (In, '(f5.1)', advance='no') A(i, j) 
      end do
      read (In, *) 
   end do
   close (In)

   open (file=output_file, encoding=E_, newunit=Out)
   do i=1, N
!      mask = .false.
      if(.not. Matched(i)) then
         forall(j=i+1:N) &
            Mask(j)=All(A(j,:) == A(i,:))
         Mask(i) = .true.
         print *, Mask
         print *, "---", Count(Mask), "---"
         if(Count(Mask) > 1) then
            write (Out, *) Pack(Ind(i:), Mask(i:))
         end if
         Matched(i:) = Matched(i:) .or. Mask(i:)
      end if
   end do

   close (Out)

end
