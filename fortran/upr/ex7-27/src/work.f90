   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, M = 0, i = 0, j = 0
   real(R_), allocatable   :: A(:,:)
   !real(R_), allocatable   ::  tmp(:)

   open (file=input_file, encoding=E_, newunit=In)
     read (In, *) N, M
     allocate (A(N, M))
!     allocate (tmp(N))
      do i = 1, N
         do j = 1, M
            read (In, '(f5.1)', advance='no') A(i, j)
         end do
         read (In, *)
      end do
   close (In)

   open (file=output_file, encoding=E_, newunit=Out)
   write(Out, *) "Source matrix:"
   do j = 1, N
      do i = 1, M
         write (Out, '(f5.1)', advance='no') A(j, i)
      end do
      write(Out, *)
   end do
   write (Out, *) "---------"

   !do i = 2, M, 2
   !    tmp = A(:,i)
   !    A(:, i) = A(:, i-1)
   !    A(:, i-1) = tmp
   !end do

   forall(i=1:M-mod(M,2):2) &
         A(:,i:i+1) = A(:,i+1:i:-1)

   write(Out, *) "columns swapped:"
   do j = 1, N
      do i = 1, M
         write (Out, '(f5.1)', advance='no') A(j, i)
      end do
      write(Out, *)
   end do
   write (Out, *) "---------"

   close (Out)

end
