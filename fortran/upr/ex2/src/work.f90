program exercise_7_24
   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, p = 0
   real(R_)                :: x = 0.0, y = 0.0, z = 0.0
   
   open (file=input_file, encoding=E_, newunit=In)
      read (In, *) x, y, z
   close (In)
  

   !A(x, y, z); MINLOC(A, dim=1)

   if (Min(x, y, z) == x) 
      p = 1
   else if (Min(x, y, z) == y) 
      p = 2
   else if (Min(x, y, z) == z) 
      p = 3
   end if
      
   open (file=output_file, encoding=E_, newunit=Out)
      write (Out, *) p
   close (Out)

end program exercise_7_24
