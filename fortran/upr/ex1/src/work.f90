program exercise_7_24
   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0
   real(R_)                :: a = 0.0, b = 0.0, c = 0.0, tmp = 0.0
   
   open (file=input_file, encoding=E_, newunit=In)
      read (In, *) a, b, c
   close (In)

   tmp = b
   b = c
   c = tmp

   tmp = a
   a = b
   b = tmp
      
   open (file=output_file, encoding=E_, newunit=Out)
      write (Out, '(f5.1)') a, b, c
   close (Out)

end program exercise_7_24
