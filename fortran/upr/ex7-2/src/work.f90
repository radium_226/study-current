   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, i = 0, pos = 0
   real(R_), allocatable   :: A(:)
   real(R_)                :: tmp = 0.0
   !logical                 :: swapped
   logical, allocatable    :: Neg(:)
   
   open (file=input_file, encoding=E_, newunit=In)
     read (In, *) N
     allocate (A(N))
     read (In, *) A
   close (In)

   !print *, "origin:", A

   Neg = A < 0

   A = [Pack(A, Neg), Pack(A, .not. Neg)]
   pos = Count(Neg) + 1 ! позиция первого положительного элемента
  
   !print *, pos
  
   !print *, "sorted:", A

   !!invert here from pos to N

   do i = 0, ABS((N - pos) / 2)
      tmp = A(N - i)
      A(N - i) = A(pos + i)
      A(pos + i) = tmp
   end do
   !print *, "invert:", A

   open (file=output_file, encoding=E_, newunit=Out)
      do i=1,N
         write(Out, '(f6.1)', advance='no') A(i)
      end do
   close (Out)

end
