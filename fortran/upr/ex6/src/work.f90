   implicit none
   integer, parameter      :: R_ = 16
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, n = 0
   real(R_)                :: x_2 = 0.0, new_cos_x = 0, cos_x = 0, x = 0, r = 0, q = 0
   
   open (file=input_file, encoding=E_, newunit=In)
      read (In, *) x
   close (In)
   
!   x_2 = x*x
!   
!   do
!      print *, elem, x_2, n, n-1
!      elem = -elem * x_2 / n * (n-1)
!      !elem = -elem * x_2 / n / (n-1)
!      
!
!      if (old_elem == elem) exit
!      old_elem = elem
!      res = res + elem
!      n = n + 2
!   end do
   r         = 1
   new_cos_x = r
   n         = 2
   x_2       = x * x
   do
      !q           = - x_2 / (2*n*(2*n + 1))
      q           = - x_2 / (n*(n-1)) 
      r           = r * q
      cos_x       = new_cos_x
      new_cos_x   = cos_x + r
      print *, n, cos_x
      n           = n + 2 !1 
      ! Продолжать, пока очередное приближение не изменится.
      if (new_cos_x == cos_x) &
         exit
   end do

   print *, cos_x

   write (Out, '(4(a, T16, "= ", f9.6/))') 'x', x, "Cos(x)", cos_x, "Fortran Cos(x)", Cos(x), "Error", cos_x - Cos(x)

   open (file=output_file, encoding=E_, newunit=Out)
!      write(Out, *) MAXVAL(B)
   close (Out)
end
