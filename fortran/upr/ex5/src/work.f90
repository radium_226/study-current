program exercise_7_24
   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, i = 0, c = 0
   real(R_), allocatable   :: A(:), B(:)
   
   open (file=input_file, encoding=E_, newunit=In)
     read (In, *) N
     allocate (A(N))
     read (In, *) A
   close (In)

   min = MaxVal(Pack(A, A<0))
!   do i = 0, N
!      if(A(i) < 0) then 
!         c = c + 1
!      end if
!   end do
!
!   allocate (B(c))
!   
!   c = 1
!   do i = 1, N
!      if(A(i) < 0) then 
!        B(c) = A((i))
!        c = c + 1
!      end if
!   end do
   
!   print *, A
!   print *, B
!   print *, MAXVAL(B)


   open (file=output_file, encoding=E_, newunit=Out)
      write(Out, *) MAXVAL(B)
   close (Out)

end program exercise_7_24
