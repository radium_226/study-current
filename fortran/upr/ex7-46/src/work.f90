   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, i = 0, j = 0
!   integer, allocatable    :: tmp(:)
   real(R_), allocatable   :: D(:), X(:), Y(:), Coord(:,:)

   open (file=input_file, encoding=E_, newunit=In)
     read (In, *) N
!     allocate (X(N))
!     allocate (Y(N))
     allocate (Coord(2, N))
     allocate (D(N))
!     allocate (tmp(N))
!     read (In, *) X
!     read (In, *) Y
!     read(In, *) Coord(1)
!     read(In, *) Coord(2)
      read(In, *) (Coord(i,:),i=1,2)
   close (In)

   !do i=1, N
   !   D(i)=sqrt(X(i)*X(i) + Y(i)*Y(i))
   !end do

   D = sqrt(Coord(1,:)**2+Coord(2,:)**2)

!   tmp = MinLoc(D)
!   print *, tmp(1)

   open (file=output_file, encoding=E_, newunit=Out)
      write(Out, *) MinLoc(D, dim=1)
   close (Out)

end
