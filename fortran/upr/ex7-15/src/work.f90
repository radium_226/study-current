   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, i = 0, j = 0
   real(R_), allocatable   :: A(:,:)

   open (file=input_file, encoding=E_, newunit=In)
     read (In, *) N
     allocate (A(N,N))
     do i = 1, N
        do j = 1, N
           read (In, '(f5.1)', advance='no') A(i,j)
        end do
        read (In, *)
     end do
   close (In)

   open (file=output_file, encoding=E_, newunit=Out)
      write(Out, *) MinVal(MaxVal(A, dim=2))
   close (Out)

end
