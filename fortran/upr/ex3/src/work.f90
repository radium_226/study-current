program exercise_7_24
   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, M = 0, i = 0, j = 0
   real(R_)                :: S = 0.0
   real(R_), allocatable   :: A(:,:), B(:,:)
   
   open (file=input_file, encoding=E_, newunit=In)
       read (In, *) N, M
       allocate (A(N, M))
       allocate (B(N, M))
!       do i = 1, N
!          do j = 1, M
!             read (In, '(f5.1)', advance='no') A(i, j)
!          end do
!          read (In, *)
!       end do
       read (In, *) (A(i, :), i = 1, N)
       read (In, *) S
   close (In)

   B = A*S

   open (file=output_file, encoding=E_, newunit=Out)
!      write (Out, '(f5.1)') A
      do j = 1, N
         do i = 1, M
            write (Out, '(f5.1)', advance='no') B(j, i)
         end do
         write(Out, *)
      end do
   close (Out)

end program exercise_7_24
