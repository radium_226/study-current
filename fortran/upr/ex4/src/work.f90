   implicit none
   integer, parameter      :: R_ = 4
   character(*), parameter :: input_file = "../data/input.txt", output_file = "output.txt", E_ = "UTF-8"
   integer                 :: In = 0, Out = 0, N = 0, iter = 0
   real(R_)                :: I = 0.0, h = 0.0, a = 0.00, b = 0.00
   real(R_), allocatable   :: X(:)

   open (file=input_file, encoding=E_, newunit=In)
      read (In, *) a, b, h
   close (In)

   N = Int((b - a) / h + .5_R_)

   allocate(X(N))
   forall(iter = 1:N) &
         X(iter) = a + (iter - 1) * h / 2
   I = sum(.8_R_ * X * exp(-(X*X + .5_R_))) * h

   open (file=output_file, encoding=E_, newunit=Out)
     write (Out, '(a, T4, "= ", f0.4)') "I", I
   close (Out)
end
