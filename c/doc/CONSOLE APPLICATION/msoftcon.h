#ifndef _INC_WCONSOLE    //don't let this file be included
#define _INC_WCONSOLE    //twice in the same source file

#include <windows.h>
#include <conio.h>

enum fstyle { SOLID_FILL, X_FILL,      O_FILL, 
              LIGHT_FILL, MEDIUM_FILL, DARK_FILL };

enum color {
   cBLACK=0,     cDARK_BLUE=1,    cDARK_GREEN=2, cDARK_CYAN=3, 
   cDARK_RED=4,  cDARK_MAGENTA=5, cBROWN=6,      cLIGHT_GRAY=7,
   cDARK_GRAY=8, cBLUE=9,         cGREEN=10,     cCYAN=11, 
   cRED=12,      cMAGENTA=13,     cYELLOW=14,    cWHITE=15 };
//--------------------------------------------------------------
int  init_console();
void set_color(color fg, color bg = cBLACK);
// x - position, y - row
void set_cursor_pos(int x, int y);
void get_cursor_pos(int &x, int &y);
void clear_screen();
void wait(int milliseconds);
void clear_line(int row);
void set_fill_style(fstyle);
//Move cursor with buttons Left, Right, Up, Down. End of moving -
// button Enter
int  click_cursor(int &x, int &y);
BOOL set_full_screen(void);

HANDLE hConsole;         //console handle
char fill_char;          //character used for fill
//--------------------------------------------------------------
int init_console()
{
  COORD console_size = {80, 25};
  //open i/o channel to console screen
  LPCWSTR FileName=L"CONOUT$";
  hConsole = CreateFileW(FileName, GENERIC_WRITE | GENERIC_READ,
                  FILE_SHARE_READ | FILE_SHARE_WRITE,
                  0L, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0L);
  if (hConsole == INVALID_HANDLE_VALUE) 
  { 
    printf("Could not open file (error %d)\n", GetLastError());
    _getch();
    return 0; 
  }  
  //set to 80x25 screen size
  SetConsoleScreenBufferSize(hConsole, console_size);
  //set text to white on black
  SetConsoleTextAttribute( hConsole, (WORD)((0 << 4) | 15) );
  fill_char = '\xDB';  //default fill is solid block
  clear_screen();
  return 1;
}
//--------------------------------------------------------------
void set_color(color foreground, color background)
{
  SetConsoleTextAttribute( hConsole, 
                       (WORD)((background << 4) | foreground) );
}  //end setcolor()

/* 0  Black          8  Dark gray
   1  Dark blue      9  Blue
   2  Dark green     10 Green
   3  Dark cyan      11 Cyan
   4  Dark red       12 Red
   5  Dark magenta   13 Magenta
   6  Brown          14 Yellow
   7  Light gray     15 White
*/
//--------------------------------------------------------------
void set_cursor_pos(int x, int y)
{
  COORD cursor_pos;              //origin in upper left corner
  cursor_pos.X = x - 1;          //Windows starts at (0, 0)
  cursor_pos.Y = y - 1;          //we start at (1, 1)
  SetConsoleCursorPosition(hConsole, cursor_pos);
}
//--------------------------------------------------------------
void clear_screen()
{
  set_cursor_pos(1, 25);
  for(int j=0; j<25; j++)
     putch('\n');
  set_cursor_pos(1, 1);
}
//--------------------------------------------------------------
void wait(int milliseconds)
{
  Sleep(milliseconds);
}
//--------------------------------------------------------------
void clear_line(int row)                    //clear to end of line
{                                 //80 spaces
  int x,y;
  get_cursor_pos(x,y);
  set_cursor_pos(1,row);
  //.....1234567890123456789012345678901234567890
  //.....0........1.........2.........3.........4 
  cputs("                                        ");
  cputs("                                        ");
  set_cursor_pos(x,y);
}
//--------------------------------------------------------------
void set_fill_style(fstyle fs)
{
  switch(fs)
  {
    case SOLID_FILL:  fill_char = '\xDB'; break;
    case DARK_FILL:   fill_char = '\xB0'; break;
    case MEDIUM_FILL: fill_char = '\xB1'; break;
    case LIGHT_FILL:  fill_char = '\xB2'; break;
    case X_FILL:      fill_char = 'X';    break;
    case O_FILL:      fill_char = 'O';    break;
  }
}
//--------------------------------------------------------------
void get_cursor_pos(int &x, int &y)
{
  CONSOLE_SCREEN_BUFFER_INFO inf;
  GetConsoleScreenBufferInfo(hConsole,&inf);
  x=inf.dwCursorPosition.X+1;
  y=inf.dwCursorPosition.Y+1;
}
//--------------------------------------------------------------
int click_cursor(int &x, int &y)
{
  int i;
  CONSOLE_CURSOR_INFO inf;
  inf.dwSize=55;
  SetConsoleCursorInfo(hConsole,&inf);
  get_cursor_pos(x,y);
  while(1)
  {
    set_cursor_pos(x,y);
    i=_getch();
    if(i==224||i==0)
      i=1000+_getch();
    switch(i)
    {
      case   27:   //Esc
        inf.dwSize=25;
        SetConsoleCursorInfo(hConsole,&inf);
        return 0;
      case   13:   //Enter
        inf.dwSize=25;
        SetConsoleCursorInfo(hConsole,&inf);
        return 1;
      case 1080:   //Down
        if(y<25)
          y++;    
        break;
      case 1072:   //Up
        if(y>1)
          y--;    
        break;
      case 1075:   //<--
        if(x>1)
          x--;    
        break;
      case 1077:   //-->
        if(x<80)
          x++;    
    }
  }
}
BOOL set_full_screen(void)
{
  COORD console_size = {80, 25};
  return SetConsoleDisplayMode(hConsole,CONSOLE_FULLSCREEN_MODE,&console_size);
}
#endif /* _INC_WCONSOLE */
