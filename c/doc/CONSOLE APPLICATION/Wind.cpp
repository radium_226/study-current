// Wind.cpp : Defines the entry point for the console application.
//

#include <conio.h>
#include <iostream>
#include "msoftcon.h"

void main(void)
{
  int x1,y1,x2,y2;
  int i,xleft,ytop,xright,ybottom;
  char str[81];
  init_console();
  clear_screen();
 
  click_cursor(x1,y1);
  click_cursor(x2,y2);
  set_color(cBLUE,cRED);
  if(x1<x2)
    {xleft=x1; xright=x2;}
   else
    {xleft=x2; xright=x1;}
   if(y1<y2)
    {ytop=y1; ybottom=y2;}
   else
    {ytop=y2; ybottom=y1;}
   for(i=0;i<xright-xleft+1;i++)
    str[i]=' ';
   str[i]='\0';
   set_color(cWHITE,cGREEN);
   for(i=ytop;i<=ybottom;i++)
   {
    set_cursor_pos(xleft,i);
    std::cout<<str;
   }
   set_color(cWHITE,cBLACK);
  getch();
}




