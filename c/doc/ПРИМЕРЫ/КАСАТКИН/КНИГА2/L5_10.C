/*
  L5_10.C
  ��⠭�������� ᮡ�⢥��� ��ࠡ��稪 ᨣ����� SIGINT � SIGABRT.
  ��᫥ ����⭮�� ������ Ctrl-C, �ணࠬ�� �����蠥���
  ���਩��. ����⢥��� ��ࠡ��稪� ᨣ����� �뢮��� ��⮢�
  ��ப� �� ��࠭.
*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <process.h>

void ctrl_c_handler(int);

int count = 0; /* ���稪 ����⨩ Ctrl-C */

/*
   ����७��� �㭪�� �ணࠬ�� L5_10.C
   ��᫥������ ����⭮� ����⨥ Ctrl-C.
*/
void ctrl_c_handler(int sig)
{
  signal(SIGINT, SIG_IGN); /* �������� ����୮� �宦�����    */
  count++;
  printf("\a%d-e ����⨥ Ctrl-C\n",count);
  if(count == 3)
    exit(0);       /* ��室 �� ���쥬 �宦����� � ��ࠡ��稪 */
  else
    /*
       ����⠭������� ॠ�樨 �� SIGINT, ���뢠���� � ���祭��
       SIG_DFL �� ������ �宦����� � ᮡ�⢥��� ��ࠡ��稪.
    */
    signal(SIGINT, ctrl_c_handler);
}

/*
   ����७��� �㭪��, �믮��塞�� �� ��室� �� �ணࠬ��.
*/
void my_exit(void)
{
  puts("����饭�� �� �ணࠬ�� my_exit() - �����襭��.");
}
int main(void)
{
 atexit(my_exit);
 if(signal(SIGINT, ctrl_c_handler) == SIG_ERR)
   {
     perror("�訡�� ��⠭���� ��ࠡ��稪� SIGINT");
     return 1;
   }
 puts("��� �����襭�� �ணࠬ�� �ਦ�� ������ Ctrl-C");
 while(1)       /* ��᪮���� 横� �������� ����� stdin       */
    getchar();  /* �믮������ �१ MS-DOS, �� ��������
		   ��᫥���� ����⨥ Ctrl-C (Ctrl-Break)       */
}
