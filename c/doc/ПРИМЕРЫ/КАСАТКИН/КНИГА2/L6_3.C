/*
  L6_3.C
  �ਭ����� � ���������� �ந����쭮� �᫮ ��ப � ࠧ��頥� ��
  � ����⨢��� �����. �४�饭�� ����� - ���� ���⮩ ��ப�.
  ������ ��� �࠭���� 㪠��⥫�� �� ��ப� � ��� �뤥�����
  �������᪨. ���ᨢ 㪠��⥫�� �ᯮ�������� � �易����
  ᯨ᪥ ������ �� STRING_NUMBER 㪠��⥫�� � ������ �����.
  ��᫥���� ������� ����� �ᯮ������ ��� 㪠��⥫� ��
  ᫥���騩 ���� 㪠��⥫��.
*/

#include<stdio.h>
#include<alloc.h>
#include<stdlib.h>
#include<string.h>

#define STOP_STRING   ""  /* ����� ��ப� ��� �����襭�� ����� */
#define BUFFER_SIZE   129 /* ���ᨬ��쭠� ��p��� + 1 ��� '\0'   */
#define STRING_NUMBER 128 /* �᫮ 㪠��⥫�� � ����� �����     */

void main(void)
{
 char buffer[BUFFER_SIZE], ** pointers, ** pointers_start, *ptr;
 unsigned n=0, index=0;
 /*
    ���p�������� ����� ��� ��p���� ����� 㪠��⥫��
    � ��ࠡ�⪠ ��������� �訡�� ��।������ �����
 */
 pointers_start = calloc(STRING_NUMBER + 1, sizeof(char*));
 if(pointers_start == NULL)
    exit(4);
 pointers = pointers_start; /* �p���� 㪠��⥫� �� ��砫� ᯨ��
			       ������ 㪠��⥫��                */
 puts("������ ⥪�⮢� ��ப�. �����襭�� - ���� ���⮩ ��ப�");
 while(strcmp(gets(buffer), STOP_STRING)) /* 横� �ਥ�� ��ப  */
    {
      /*
	 ����⪠ ��।������ ����� � ��ࠡ�⪠ ���������
	 �訡��.  ����訢����� �᫮ ����, ����襥 �� 1 �����
	 ��������� ��ப�, ⠪ ��� ᨬ��� '\0' ⠪��
	 ��p������.
      */
      if((ptr = malloc(strlen(buffer) + 1)) == NULL)
	 {
	   printf("��������� ᢮������ ������\n");
	   break;
	 }
      strcpy(ptr, buffer); /* ��७�� � ���� ��������� ��ப�   */
      if(index == STRING_NUMBER)/*�᫨ �� 㪠��⥫� �����
				   �p��� ������ �ᯮ�짮����*/
	 {
	   /*
	      ���p������ ������ ��� ᫥���饣� ����� 㪠��⥫��
	      � ����頥� 㪠��⥫� �� ���� � ��᫥���� �������
	      �p����饣� ����� 㪠��⥫��
	   */
	   pointers[STRING_NUMBER] =
			   calloc(STRING_NUMBER +1, sizeof(char*));
	   if(pointers[STRING_NUMBER] == NULL)
	     {
	      printf("��������� ᢮������ ������\n");
	      break;
	     }
	   index = 0; /* 㪠��⥫� � ����� ����� �ᯮ������� � 0 */
           pointers = (char**)pointers[STRING_NUMBER];
         }
      pointers[index++] = ptr; /* ������ � ���ᨢ 㪠��⥫� �� 
                                  ��砫� ��ப� � ���         */
      n++;         /* ���騢����� ��饥 �᫮ ��������� ��p�� */
    }              /* ����� ⥫� 横�� �ਥ�� ��ப            */
 printf("�p��� ��p�� ����p襭. �p���� %d ��p��:\n", n);
 /*
    �뢮� �� ��࠭ ��p�� �� 㪠��⥫� �� ��� � ���.
 */
 for(index = 0, pointers = pointers_start; n > 0; n--)
    {
      puts(pointers[index++]);
      if(index == STRING_NUMBER) /* �ᯮ�짮��� ���� ���� 㪠��⥫��? */ 
         {
           index = 0;
           pointers = (char**) pointers[STRING_NUMBER];
         }
    }
}
