/*
   L4_11.C
   �뤠�� �� �⠭����� �뢮� ���� � �६� 䠩��, ��⥬
   ����訢��� � ���������� ���� ���祭�� ���� � �६��� 䠩��
   � ��⠭�������� ��.
*/

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <io.h>

int main(void)
{
  int handle;
  unsigned f_year, f_month, f_day, f_hour, f_minute, f_second;
  struct ftime old_date_time, new_date_time;
  char pathname[128], prompt;
  do
     {
       fputs("������ ��� 䠩��: ", stdout);
       fflush(stdin);
       fgets(pathname, 128, stdin);
       pathname[strlen(pathname) - 1] = '\0';
       if((handle = _open(pathname, O_RDONLY)) != -1)
         {
           /*
              ��।���� � �ᯥ�⠥� ����/�६� 䠩��.
           */
           getftime(handle, &old_date_time);
	   printf("���: %02u-%02u-%04u. "\
           "�६�: %02u:%02u:%02u.\n",
           old_date_time.ft_day, old_date_time.ft_month,
           old_date_time.ft_year + 1980,
           old_date_time.ft_hour, old_date_time.ft_min,
           old_date_time.ft_tsec * 2);
           /*
              ����� ����� ���� � �६���.
           */
	   printf("������ ����� ���� � �ଠ� ��-��-��: ");
           fflush(stdin);
           scanf("%u-%u-%u", &f_day, &f_month, &f_year);
           printf("������ ����� �६� � �ଠ� ��-��-��: ");
           fflush(stdin);
           scanf("%u-%u-%u", &f_hour, &f_minute, &f_second);
           /*
              ������塞 ���� ������୮� ��६����� new_date_time.
           */
	   new_date_time.ft_tsec  = f_second/2;
           new_date_time.ft_min   = f_minute;
           new_date_time.ft_hour  = f_hour;
           new_date_time.ft_day   = f_day;
           new_date_time.ft_month = f_month;
           new_date_time.ft_year  = f_year - 80;
           /*
              ��⠭�������� ���� ����/�६�.
           */
	   if(setftime(handle, &new_date_time))
             perror("\a��� � �६� �� ��������");
           close(handle);
         }
       else
         perror("\a�訡�� ������ 䠩��");
       fputs("�த������ ? (Y/N)", stdout);
       fflush(stdin);
     }
  while((prompt = toupper(getchar())) == 'Y');
  return(0);
}
