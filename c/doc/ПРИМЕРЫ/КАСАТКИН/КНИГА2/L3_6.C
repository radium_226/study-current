/*
  L3_6.C
  ����७��� �㭪樨 ��� ����� ������୮� ��६����� ��
  蠡���� BOOKS �� ����⮣� 䠩�� � �� �뢮�� � ������ 䠩�.
  ������ -  EOF, �� ���⨦���� ���� 䠩��;
             int n - �᫮ ��७�ᥭ��� ����;
*/
#include <stdio.h>
#include <string.h>
#include <io.h>
#define NAME  32
#define TITLE 64

typedef struct  { /* ���譨� 蠡��� ����� ���� � 䠩�� */
                      char name[32];
                      char title[64];
                      int year;
                      float price;
                } BOOKS;
int file_input_book(FILE * fp, BOOKS * ptr)
{
  return(fread(ptr, sizeof(BOOKS), 1, fp));
}
int file_output_book(FILE * fp, BOOKS * ptr)
{
  return(fwrite(ptr, sizeof(BOOKS), 1, fp));
}
