/*
  L4_1NEW.C
*/
#include <stdio.h>
#define TRUE   "������"
#define FALSE  "����"
void main(void)
{
     float var1, var2;
     printf("������ ��ࢮ� ���祭�� (var1): ");
     scanf("%f", &var1);
     printf("������ ��஥ ���祭�� (var2): ");
     scanf("%f", &var2);
     printf("var1 >  var2 �� %s\n", var1 > var2 ? TRUE : FALSE);
     printf("var1 <  var2 �� %s\n", var1 < var2 ? TRUE : FALSE);
     printf("var1 == var2 �� %s\n", var1 == var2 ? TRUE : FALSE);
     printf("var1 >= var2 �� %s\n", var1 >= var2 ? TRUE : FALSE);
     printf("var1 <= var2 �� %s\n", var1 <= var2 ? TRUE : FALSE);
     printf("var1 != var2 �� %s\n", var1 != var2 ? TRUE : FALSE);
     printf("var1 || var2 �� %s\n", var1 || var2 ? TRUE : FALSE);
     printf("var1 && var2 �� %s\n", var1 && var2 ? TRUE : FALSE);
     printf("!var1 �� %s\n", !var1 ? TRUE : FALSE);
     printf("!var2 �� %s\n", !var2 ? TRUE : FALSE);
}
