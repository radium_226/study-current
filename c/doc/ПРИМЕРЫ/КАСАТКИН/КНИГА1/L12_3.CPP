//
// L12_3.CPP
// �ਬ�� ���������, �ᯮ����饣� ������ new.
//

#include <iostream.h>
#include <string.h>

class string {
      char * str;
      int max_length;
public:
      string(int size)  // ��������� ��ꥪ⮢ ����� string
	 {
	   str = new char[size];
	   max_length = size;
	 }
      ~string()  // �������� ��ꥪ⮢ ����� string
	 {
	   delete str;
	 }
      int input(void);
      void output(void);
};

int string::input(void)
{
  cout << "������ �ந������� ��ப� �� ����� ";
  cout << max_length << " ᨬ�����\n";
  cin >> str;
  return strlen(str);
}

void string::output(void)
{
  cout << str;
  cout << '\n';
}

void main(void)
{
  int length;
  string my = string(128); // ᮧ���� ��ꥪ� my ����� string;
			   // ��������� �ॡ�� ��ࠬ���
  length = my.input();
  cout << "��������� ��ப� ����� ����� ";
  cout << length << " ᨬ�����:\n";
  my.output();
}
