/*
   L10_3.C
   �ਬ�� �ᯮ�짮����� ��⮢�� ����� � ��������.
*/

#include <stdio.h>

void main(void)
{
  struct EXAMPLE {

		   int      i: 2;
		   unsigned j: 2;
		   int       : 2;
		   int      k: 2;
		   int  dummy: 8;
		 } my_struct;
  my_struct.dummy = 0;
  my_struct.i =  1;
  my_struct.j =  3;
  my_struct.k = -1;
  printf("%x\n", my_struct);
  printf("�⤥��� ����: %d %u %d\n",
          my_struct.i, my_struct.j, my_struct.k);
}
