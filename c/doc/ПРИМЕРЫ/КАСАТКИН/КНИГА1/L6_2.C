/*
  L6_2.C
  � ������ ⥬, �� �� ��।��� "���� �㤥�".  �ਭ�����
  ᨬ���� � ���������� � ��।���� �⭮�⥫��  �����
  ���᪨� �㪢. �����襭�� �ணࠬ�� - ���� ᨬ���� '$'.  ���
  ⮣�, �⮡� �멣��� ��⮬�����, �㦭� ����� ��� ����� �����
  ��ப ⥪��.
*/
#include<stdio.h>
#include<conio.h>
void main(void)
{
 char lower_case[] = "��������������������������������";
 char upper_case[] = "��������������������������������";
 static float frequency[sizeof(lower_case)], total=0.;
 char ch;
 int index;
 /*
    ���� �ਥ�� ᨬ�����. �����襭�� - ���� ᨬ���� '$'.
 */
 puts("���� ���⠩� ⥪�� �� ���᪮� �몥. �����襭�� - ᨬ��� '$'\n");
 while((ch = getchar()) != '$')
    {
      /*
         ��।��塞 ���浪��� ����� ���᪮� �㪢�.
      */
      for(index = 0; index < 32; index++)
         if((lower_case[index] == ch) || (upper_case[index] == ch))
           {
             /*
		������� ᮢ�������.
	     */
	     total++;
	     frequency[index]++;
	     break; /* ��室 �� 横�� */
	   }
	 else
	   continue;
    }
 /*
    �����襭�� 横�� ����� ᨬ�����.
    ��।������ �⭮�⥫��� ���� ᨬ�����.
  */
  puts("\n*******  �⭮�⥫�� ����� ���᪨� �㪢 *******\n");
  if(total)
     {
       for(index = 0; index < 32; index++)
	 {
	   frequency[index] = frequency[index]*100./total;
	   printf("%c - %5.1f ", upper_case[index], frequency[index]);
	 }
     }
  else
     puts("\a�� ���� ���᪠� �㪢� �� �����㦥��, ����⨪� ���������.");
}
