#include <stdio.h>
#include <termios.h>
#include <unistd.h>

char* ReplaceSymbol(char* str, char sym, char newsym){
   for(int i=0; str[i]; i++){
      if(str[i] == sym)
         str[i] = newsym;
   }

   return str;
}


int main(){
   char str[100], sym, newsym;

   printf("%s", "Введите строку:\n");
   scanf("%s", str);

   printf("%s", "Введите символ для замены:\n");

   sym = getche();
   printf("%s", ReplaceSymbol(str, sym, newsym));
 
   return 0;
}
